import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?


  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    
    setSwaybeatAppearance()
    
    window = UIWindow(frame: UIScreen.mainScreen().bounds)
    window!.rootViewController = RootTabBarController()
    window!.makeKeyAndVisible()
    
    return true
  }

}

