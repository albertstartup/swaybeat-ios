import UIKit

class RootTabBarController: UITabBarController {
  
  override func viewDidLoad() {
    viewControllers = [ShareNavigationController()]
  }
  
}
