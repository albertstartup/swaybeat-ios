import UIKit

class SearchTableViewController: UITableViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let searchBar = UISearchBar()
    searchBar.placeholder = "Search For A Song"
        
    navigationItem.titleView = searchBar
    
    tableView.separatorInset = UIEdgeInsetsZero
    tableView.keyboardDismissMode = .OnDrag
  }
}
