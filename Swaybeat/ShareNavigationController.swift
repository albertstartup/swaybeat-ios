import UIKit

class ShareNavigationController: UINavigationController {
  
  init() {
    super.init(nibName: nil, bundle: nil)
    
    let tabBarItemImage = UIImage(named: "ShareTabBarIcon")
    tabBarItem = UITabBarItem(title: "Share", image: tabBarItemImage, tag: 1)
  }
  
  override func viewDidLoad() {
    pushViewController(SearchTableViewController(), animated: true)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
