import UIKit

let SwaybeatPink = UIColor(red: 0.91, green: 0.012, blue: 0.39, alpha: 1.0);

func setSwaybeatAppearance() {
  
  UINavigationBar.appearance().barTintColor = SwaybeatPink
  UINavigationBar.appearance().tintColor = .whiteColor()
  UINavigationBar.appearance().translucent = false
  UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]

  UITabBar.appearance().backgroundColor = .whiteColor()
  UITabBar.appearance().tintColor = SwaybeatPink
  UITabBar.appearance().translucent = false

}