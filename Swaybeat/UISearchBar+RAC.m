//
//  UISearchBar+RAC.m
//  ReactiveCocoaExample
//
//  Created by Justin DeWind on 1/26/14.
//  Copyright (c) 2014 Justin DeWind. All rights reserved.
//

#import "UISearchBar+RAC.h"
#import <objc/runtime.h>

@interface UISearchBar()<UISearchBarDelegate>

@end
@implementation UISearchBar (RAC)
- (RACSignal *)rac_textSignal {
  RACSignal *signal = objc_getAssociatedObject(self, _cmd);
  if (signal != nil) return signal;
  signal = [[self rac_signalForSelector:@selector(searchBar:textDidChange:) fromProtocol:@protocol(UISearchBarDelegate)] map:^id(RACTuple *tuple) {
    return tuple.second;
  }];
  self.delegate = self; // Setting delegate to self should come after rac_signalForSelector
  objc_setAssociatedObject(self, _cmd, signal, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
  return signal;
}
@end